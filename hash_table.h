#pragma once

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define __DEFAULT_ARRAY_LEN 10
#define __MAX_LOAD_FACTOR 0.8f

typedef struct __Cell {
	char* key;
	int value;
	struct __Cell* next;
} __Cell;

typedef struct {
	__Cell** __bucket_array;
	size_t __array_len;
	size_t __size;
} HashTable;


size_t __string_hash(char* string) {
	size_t hash = 5381;
	int c;
	while (c = *string++)
		hash = ((hash << 5) + hash) + c;

	return hash;
}


__Cell* __alloc_cell(char* key, int value) {
	__Cell* new_cell = malloc(sizeof(__Cell));
	new_cell->key = malloc(strlen(key) + 1);
	strcpy(new_cell->key, key);
	new_cell->value = value;
	new_cell->next = NULL;
	return new_cell;
}


void __free_buckets(__Cell** arr, size_t len) {
	for (size_t i = 0; i < len; i++) {
		__Cell* current = arr[i];
		while (current) {
			__Cell* temp = current->next;
			free(current->key);
			free(current);
			current = temp;
		}
	}

	free(arr);
}


void __insert_into_array(__Cell** arr, size_t len, size_t* size, char* key, int value) {
	size_t hash = __string_hash(key) % len;
	if (!arr[hash]) {
		__Cell* new_cell = __alloc_cell(key, value);
		arr[hash] = new_cell;
	}
	else {
		__Cell* current = arr[hash];
		while (current->next)
			current = current->next;

		__Cell* new_cell = __alloc_cell(key, value);
		current->next = new_cell;
	}

	++*size;
}


void __reallocate(HashTable* ht) {
	size_t new_len = ht->__array_len * 2 + 1;
	size_t size = 0;
	__Cell** new_array = calloc(new_len, sizeof(__Cell*));
	for (size_t i = 0; i < ht->__array_len; i++) {
		__Cell* current = ht->__bucket_array[i];
		while (current) {
			__insert_into_array(new_array, new_len, &size, current->key, current->value);
			current = current->next;
		}
	}

	__free_buckets(ht->__bucket_array, ht->__array_len);
	ht->__bucket_array = new_array;
	ht->__array_len = new_len;
	ht->__size = size;
}


HashTable HashTable_new() {
	HashTable ret = {.__size = 0, .__array_len = __DEFAULT_ARRAY_LEN, .__bucket_array = calloc(__DEFAULT_ARRAY_LEN, sizeof(__Cell*))};
	return ret;
}


void HashTable_free(HashTable* ht) {
	__free_buckets(ht->__bucket_array, ht->__array_len);
}


size_t HashTable_size(HashTable* ht) {
	return ht->__size;
}

void HashTable_insert(HashTable* ht, char* key, int value) {
	__insert_into_array(ht->__bucket_array, ht->__array_len, &(ht->__size), key, value);
	if ((float)ht->__size / (float)ht->__array_len > __MAX_LOAD_FACTOR)
		__reallocate(ht);
}


int HashTable_get(HashTable* ht, char* key) {
	size_t hash = __string_hash(key) % ht->__array_len;
	__Cell* current = ht->__bucket_array[hash];
	while (current) {
		if (strcmp(current->key, key) == 0)
			return current->value;

		current = current->next;
	}

	return -1;
}


bool HashTable_contains(HashTable* ht, char* key) {
	size_t hash = __string_hash(key) % ht->__array_len;
	__Cell* current = ht->__bucket_array[hash];
	while (current) {
		if (strcmp(current->key, key) == 0)
			return true;
		
		current = current->next;
	}

	return false;
}

/*
	Brainfuck interpreter
	by Duncan Sparks
*/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define ARRAY_SIZE 30000
#define INPUT_LIMIT ARRAY_SIZE
#define LOOP_LIMIT 256

int main() {
	char array[ARRAY_SIZE] = {0};
	char* ptr = array;
	
	char input[INPUT_LIMIT];
	fgets(input, INPUT_LIMIT, stdin);

	int loops[LOOP_LIMIT];
	int loop_level = 0;

	bool comment = false;

	for (int i = 0; i < strlen(input) - 1; i++) {
		if (!comment || i == 0 || input[i] == ']') {
			switch (input[i]) {
				case '>':
					++ptr;
					break;
				case '<':
					--ptr;
					break;
				case '+':
					++*ptr;
					break;
				case '-':
					--*ptr;
					break;
				case '.':
					putchar(*ptr);
					break;
				case ',':
					*ptr = getchar();
					break;
				case '[': {
					if (i == 0)
						comment = true;
					else {
						loops[loop_level] = i;
						loop_level++;
					}

					break;
				}
				case ']': {
					if (comment)
						comment = false;
					else {
						if (*ptr)
							i = loops[loop_level - 1];
						else
							loop_level--;
					}
					
					break;
				}
			}
		}
	}
}

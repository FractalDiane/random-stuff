/*
	Turing machine emulator
	by Duncan Sparks
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define TAPE_LENGTH 30000
#define TAPE_START 15000

#define FILENAME_LENGTH 64
#define FILE_LINE_LENGTH 256

#define INPUT_LENGTH 4096

typedef struct {
	int pre_state;
	char seen_char;
	int new_state;
	char write_char;
	bool move_dir;
} Instruction;


int main()
{
	/* READ INSTRUCTIONS */
	char tape[TAPE_LENGTH];
	memset(tape, '_', TAPE_LENGTH);
	
	Instruction** instructions;
	int* array_lens;

	char in_file[FILENAME_LENGTH];
	printf("Enter instruction file: ");
	fgets(in_file, FILENAME_LENGTH, stdin);
	in_file[strlen(in_file) - 1] = '\0';

	FILE* file = fopen(in_file, "r");

	int num_batches;
	fscanf(file, "%i", &num_batches);
	instructions = calloc(num_batches, sizeof(Instruction*));
	array_lens = calloc(num_batches, sizeof(int));

	fseek(file, 1, SEEK_CUR);

	int accept;
	int reject;
	fscanf(file, "%i,%i", &accept, &reject);

	fseek(file, 2, SEEK_CUR);

	char line[FILE_LINE_LENGTH];
	int current_batch = -1;
	int current_subbatch = 0;
	int current_batch_len = 0;
	while (fgets(line, FILE_LINE_LENGTH, file)) {
		if (strlen(line) > 1) {
			if (strlen(line) == 2) {
				current_batch++;
				current_subbatch = 0;
				current_batch_len = line[0] - '0';
				array_lens[current_batch] = current_batch_len;
				instructions[current_batch] = calloc(current_batch_len, sizeof(Instruction));
			}
			else {
				int from;
				char seen;
				int to;
				char write;
				char dir;

				sscanf(line, "%i,%c -> %i,%c,%c", &from, &seen, &to, &write, &dir);

				Instruction new_instr = {from, seen, to, write, dir == 'R'};
				instructions[current_batch][current_subbatch] = new_instr;

				current_subbatch++;
			}
		}
	}

	fclose(file);

	/* READ INPUT STRING */
	char input[INPUT_LENGTH];
	printf("Enter input string: ");
	fgets(input, INPUT_LENGTH, stdin);
	strncpy(tape + TAPE_START, input, INPUT_LENGTH);

	/* PROCESS */
	int accepted = -1;
	int state = 0;
	int tape_head = TAPE_START;

	while (accepted == -1) {
		Instruction* current_array = instructions[state];	
		for (int i = 0; i < array_lens[state]; i++) {
			if (tape[tape_head] == '\n')
				tape[tape_head] = '_';

			if (tape[tape_head] == current_array[i].seen_char) {
				state = current_array[i].new_state;
				tape[tape_head] = current_array[i].write_char;
				tape_head = current_array[i].move_dir ? ++tape_head : --tape_head;

				if (state == accept)
					accepted = 1;
					
				if (state == reject)
					accepted = 0;
					
				break;
			}
		}
	}

	if (accepted == 1)
		printf("String was ACCEPTED\n");
	else
		printf("String was REJECTED\n");


	/* CLEAN UP */
	for (int i = 0; i < num_batches; i++)
		free(instructions[i]);

	free(instructions);
	free(array_lens);
}
